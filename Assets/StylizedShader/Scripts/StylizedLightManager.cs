﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class StylizedLightManager : MonoBehaviour
{
    public Material[] m_Materials;

    public Light m_FragDirectionLight = null;
    public Light m_FragPointLight = null;
    public Light m_VertDirectionLight0 = null;
    public Light m_VertDirectionLight1 = null;
    public Light m_VertDirectionLight2 = null;
    public Light m_VertDirectionLight3 = null;
    public Light m_VertDirectionLight4 = null;
    public Light m_VertDirectionLight5 = null;


    void LateUpdate ()
    {
        if (m_Materials == null)
        {
            return;
        }

        for (int i = 0; i < m_Materials.Length; ++i)
        {
            Material mat = m_Materials[i];
            if (mat == null)
            {
                continue;
            }

            mat.SetColor("_ambientvert_light_color", RenderSettings.ambientLight);

            if (m_FragDirectionLight != null && m_FragDirectionLight.isActiveAndEnabled)
            {
                Vector3 forward = m_FragDirectionLight.transform.forward;
                Color light = new Vector4(m_FragDirectionLight.color.r, m_FragDirectionLight.color.g, m_FragDirectionLight.color.b, m_FragDirectionLight.intensity);

                mat.SetVector("_dirspec_light_direction", forward );                
                mat.SetColor("_dirspec_light_color", light);
                
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirspec_light_direction", forward);
                mat.SetColor("_dirspec_light_color", light);
            }

            if (m_FragPointLight != null && m_FragPointLight.isActiveAndEnabled)
            {
                Vector3 pos = m_FragPointLight.transform.position;
                Vector4 light = new Vector4(m_FragPointLight.color.r, m_FragPointLight.color.g, m_FragPointLight.color.b, m_FragPointLight.intensity);
                float radius = m_FragPointLight.range;

                mat.SetVector("_pointspec_light_position", pos);
                mat.SetColor("_pointspec_light_color", light);
                mat.SetFloat("_pointspec_light_radius", radius);
            }
            else
            {
                Vector3 pos = Vector3.zero;
                Vector4 light = Vector4.zero;
                float radius = 1.0f;

                mat.SetVector("_pointspec_light_position", pos);
                mat.SetColor("_pointspec_light_color", light);
                mat.SetFloat("_pointspec_light_radius", radius);
            }

            if (m_VertDirectionLight0 != null && m_VertDirectionLight0.isActiveAndEnabled )
            {
                Vector3 forward = m_VertDirectionLight0.transform.forward;
                Vector4 light = new Vector4( m_VertDirectionLight0.color.r, m_VertDirectionLight0.color.g, m_VertDirectionLight0.color.b, m_VertDirectionLight0.intensity );
                
                mat.SetVector("_dirvert_light_direction_0", forward);
                mat.SetVector("_dirvert_light_color_0", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_0", forward);
                mat.SetVector("_dirvert_light_color_0", light);
            }


            if (m_VertDirectionLight1 != null && m_VertDirectionLight1.isActiveAndEnabled)
            {
                Vector3 forward = m_VertDirectionLight1.transform.forward;
                Vector4 light = new Vector4(m_VertDirectionLight1.color.r, m_VertDirectionLight1.color.g, m_VertDirectionLight1.color.b, m_VertDirectionLight1.intensity);

                mat.SetVector("_dirvert_light_direction_1", forward);
                mat.SetVector("_dirvert_light_color_1", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_1", forward);
                mat.SetVector("_dirvert_light_color_1", light);
            }


            if (m_VertDirectionLight2 != null && m_VertDirectionLight2.isActiveAndEnabled)
            {
                Vector3 forward = m_VertDirectionLight2.transform.forward;
                Vector4 light = new Vector4(m_VertDirectionLight2.color.r, m_VertDirectionLight2.color.g, m_VertDirectionLight2.color.b, m_VertDirectionLight2.intensity);

                mat.SetVector("_dirvert_light_direction_2", forward);
                mat.SetVector("_dirvert_light_color_2", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_2", forward);
                mat.SetVector("_dirvert_light_color_2", light);
            }


            if (m_VertDirectionLight3 != null && m_VertDirectionLight3.isActiveAndEnabled)
            {
                Vector3 forward = m_VertDirectionLight3.transform.forward;
                Vector4 light = new Vector4(m_VertDirectionLight3.color.r, m_VertDirectionLight3.color.g, m_VertDirectionLight3.color.b, m_VertDirectionLight3.intensity);

                mat.SetVector("_dirvert_light_direction_3", forward);
                mat.SetVector("_dirvert_light_color_3", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_3", forward);
                mat.SetVector("_dirvert_light_color_3", light);
            }


            if (m_VertDirectionLight4 != null && m_VertDirectionLight4.isActiveAndEnabled)
            {
                Vector3 forward = m_VertDirectionLight4.transform.forward;
                Vector4 light = new Vector4(m_VertDirectionLight4.color.r, m_VertDirectionLight4.color.g, m_VertDirectionLight4.color.b, m_VertDirectionLight4.intensity);

                mat.SetVector("_dirvert_light_direction_4", forward);
                mat.SetVector("_dirvert_light_color_4", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_4", forward);
                mat.SetVector("_dirvert_light_color_4", light);
            }


            if (m_VertDirectionLight5 != null && m_VertDirectionLight5.isActiveAndEnabled)
            {
                Vector3 forward = m_VertDirectionLight5.transform.forward;
                Vector4 light = new Vector4(m_VertDirectionLight5.color.r, m_VertDirectionLight5.color.g, m_VertDirectionLight5.color.b, m_VertDirectionLight5.intensity);

                mat.SetVector("_dirvert_light_direction_5", forward);
                mat.SetVector("_dirvert_light_color_5", light);
            }
            else
            {
                Vector3 forward = Vector3.forward;
                Vector4 light = Vector4.zero;

                mat.SetVector("_dirvert_light_direction_5", forward);
                mat.SetVector("_dirvert_light_color_5", light);
            }
        }
    }
}
