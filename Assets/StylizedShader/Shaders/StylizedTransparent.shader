Shader "Stylized/Transparent" 
{
	Properties
	{
		_surface_color("Surface Color", Color) = (0.5, 0.5, 0.5, 0.25)
		_surface_alpha("Surface Alpha", Range(0,1)) = 1
		
		[Header(Texture Map)]
		[Toggle(TEXTURE_MAP)]
		_EnableTextureMap("Enable Texture Map", Int) = 1
		_texturemap("Texture Map", 2D) = "white" {}
		
		[Header(Ambient Light)]
		[Toggle(AMBIENT_VERTEX_LIGHT)]
		_EnableAmbientLight("Enable Ambient Vertex Light", Int) = 1
		_ambientvert_light_color("Ambient Light Color", Color) = (0,0,0,0)
		
		[Header(Direction Vertex Lights)]
		[Toggle(DIRECTION_VERTEX_LIGHTS)]
		_EnableDirectionVertexLights("Enable Direction Vertex Lights", Int) = 1
		_dirvert_light_color_0("[0] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_0("[0] Direction (XYZ)", Vector) = (0,0,1,0)
		_dirvert_light_color_1("[1] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_1("[1] Direction (XYZ)", Vector) = (0,0,1,0)
		_dirvert_light_color_2("[2] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_2("[2] Direction (XYZ)", Vector) = (0,0,1,0)
		_dirvert_light_color_3("[3] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_3("[3] Direction (XYZ)", Vector) = (0,0,1,0)
		_dirvert_light_color_4("[4] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_4("[4] Direction (XYZ)", Vector) = (0,0,1,0)
		_dirvert_light_color_5("[5] Color (RGB,Intensity)", Color) = (0,0,0,0)
		_dirvert_light_direction_5("[5] Direction (XYZ)", Vector) = (0,0,1,0)
		
		[Header(Directional Specular Light)]
		[Toggle(DIRECTION_SPEC_LIGHT)]
		_EnableDirectionSpecLight("Enable Direction Spec Light", Float) = 0
		_dirspec_light_direction("Direction Spec Light: Direction", Vector) = (0,0,1,0)
		_dirspec_light_color("Direction Spec Light: Color", Color) = (1,1,1,1)
		_dirspec_light_spec_k("Direction Spec Light: Spec K", Range(0.01,1) ) = 1
		_dirspec_light_spec_color("Direction Spec Light: Spec Color", Color) = (1,1,1,1)

		[Header(Point Specular Light)]
		[Toggle(POINT_SPEC_LIGHT)]
		_EnablePointSpecLight("Enable Point Spec Light", Float) = 0
		_pointspec_light_position("Point Spec Light: Direction", Vector) = (0,0,1,0)
		_pointspec_light_color("Point Spec Light: Color", Color) = (1,1,1,1)
		_pointspec_light_radius("Point Spec Light: Radius", Range(0.01,1000)) = 10
		_pointspec_light_spec_k("Point Spec Light: Spec K", Range(0.01,1)) = 1		
		_pointspec_light_atten_terms("Point Spec Light Attenuation (constant, linear, quadratic, radius): ", Vector) = (1,1,1,0)
		_pointspec_light_spec_color("Point Spec Light: Spec Color", Color) = (1,1,1,1)

		[Header(Reflection Map)]
		[Toggle(REFLECTION_MAP)]
		_EnableReflectionMap("Enable Reflection Map", Float) = 0
		_cubemap("Reflection Map", 2D) = "black" {}
		
		[Header(Emission Map)]
		[Toggle(EMISSION_MAP)]
		_EnableEmissionMap("Enable Emission Map", Int) = 1
		_emissionmap("Emission Map", 2D) = "black" {}
		_emission_color("Emission Color", Color) = (1,1,1,0)
		_emission_intensity("Emission Intensity", Range(0,10)) = 1
		
		[Header(Fog)]		
		[Toggle(DISTANCE_FOG)]
		_EnableDistanceFog("Enable Distance Fog", Int) = 1	
		_distfog_color("Distance Fog Color", Color) = (1,1,1,0)
		_distfog_params("Distance Fog (Range, Scale, Exponent)", Vector) = (1,1,1,0)

		[Toggle(HEIGHT_FOG)]
		_EnableHeightFog("Enable Height Fog", Int) = 1
		_heightfog_color("Height Fog Color", Color) = (1,1,1,0)
		_heightfog_top("Height Fog Top", Float) = 0
		_heightfog_bottom("Height Fog Bottom", Float) = 0
	}


	SubShader
	{
		Tags
		{
			"IgnoreProjector" = "True"
			"Queue" = "Transparent"
		}

		Pass
		{
			Name "FrontPass"
			Tags
			{
				"LightMode" = "ForwardBase"
			}

			Blend  SrcAlpha OneMinusSrcAlpha
			Cull Back
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma shader_feature _ AMBIENT_VERTEX_LIGHT
			#pragma shader_feature _ DIRECTION_SPEC_LIGHT
			#pragma shader_feature _ POINT_SPEC_LIGHT
			#pragma shader_feature _ REFLECTION_MAP
			#pragma shader_feature _ DISTANCE_FOG
			#pragma shader_feature _ HEIGHT_FOG
			#pragma shader_feature _ DIRECTION_VERTEX_LIGHTS
			#pragma shader_feature _ TEXTURE_MAP
			#pragma shader_feature _ EMISSION_MAP

			#pragma multi_compile_fwdadd
			#pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
			#pragma target 3.0

			#define UNITY_PASS_FORWARDADD
			#include "UnityCG.cginc"

			//*************************************
			half4 _surface_color;
			half _surface_alpha;
			
			uniform sampler2D _texturemap;
			uniform float4 _texturemap_ST;
			
			half4 _ambientvert_light_color;
			
			half4 _dirvert_light_color_0;
			half4 _dirvert_light_color_1;
			half4 _dirvert_light_color_2;
			half4 _dirvert_light_color_3;
			half4 _dirvert_light_color_4;
			half4 _dirvert_light_color_5;

			float3 _dirvert_light_direction_0;
			float3 _dirvert_light_direction_1;
			float3 _dirvert_light_direction_2;
			float3 _dirvert_light_direction_3;
			float3 _dirvert_light_direction_4;
			float3 _dirvert_light_direction_5;
			
			float4 _dirspec_light_direction;
			half4 _dirspec_light_color;
			float _dirspec_light_spec_k;
			half4 _dirspec_light_spec_color;
			
			float4 _pointspec_light_position;
			half4 _pointspec_light_color;
			float _pointspec_light_radius;
			float  _pointspec_light_spec_k;
			float4 _pointspec_light_atten_terms;
			half4 _pointspec_light_spec_color;

			uniform sampler2D _cubemap;
			uniform float4 _cubemap_ST;
			
			uniform sampler2D _emissionmap;
			uniform float4 _emissionmap_ST;
			half4 _emission_color;
			float _emission_intensity;

			half4 _distfog_color;
			float4 _distfog_params;

			half4 _heightfog_color;
			float _heightfog_top;
			float _heightfog_bottom;
			//*************************************
			
			//*************************************
			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};
			//*************************************
			
			//*************************************
			struct VertexOutput
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
				half3 vertColor : COLOR0;
				half3 vertLighting : COLOR1;
			};
			//*************************************


			//---------------------------------------------------------------------------
			float Lerp(float a, float b, float t) 
			{
				return t*b + (1 - t)*a;
			}
			

			//---------------------------------------------------------------------------
			float Map(float t, float t0, float t1, float A, float B)
			{
				return A + (t - t0) * (B - A) / (t1 - t0);
			}


			//---------------------------------------------------------------------------
			float InvLerp(float A, float B, float C)
			{
				return (C - A) / (B - A);
			}


			//---------------------------------------------------------------------------
			VertexOutput vert(VertexInput v)
			{
				VertexOutput o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.normalDir = UnityObjectToWorldNormal(v.normal);
				o.uv = TRANSFORM_TEX(v.texcoord, _texturemap);

				o.vertColor = v.color * _surface_color.rgb;

// Per-vertex directional lights. alpha is intensity
#if DIRECTION_VERTEX_LIGHTS
				o.vertLighting  = _dirvert_light_color_0.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_0.xyz)) * _dirvert_light_color_0.a;
				o.vertLighting += _dirvert_light_color_1.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_1.xyz)) * _dirvert_light_color_1.a;
				o.vertLighting += _dirvert_light_color_2.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_2.xyz)) * _dirvert_light_color_2.a;
				o.vertLighting += _dirvert_light_color_3.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_3.xyz)) * _dirvert_light_color_3.a;
				o.vertLighting += _dirvert_light_color_4.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_4.xyz)) * _dirvert_light_color_4.a;
				o.vertLighting += _dirvert_light_color_5.rgb * saturate(dot(o.normalDir.xyz, -_dirvert_light_direction_5.xyz)) * _dirvert_light_color_5.a;
#endif

				return o;
			}


			//---------------------------------------------------------------------------
			float4 frag(VertexOutput i) : COLOR
			{
// Build a bunch of common per-framgent data
				float3 ViewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 Normal = normalize(i.normalDir.xyz);

// base surface color 
				float3 SurfaceColor = i.vertColor;
#if TEXTURE_MAP
				SurfaceColor *= tex2D(_texturemap, i.uv);
#endif

// Our odd 'reflection' map
#if REFLECTION_MAP
				float3 ViewReflect = reflect(ViewDirection, Normal);
				float2 ReflectionLookup = ViewReflect.rg;
				float3 ReflectionMapColor = tex2D(_cubemap, TRANSFORM_TEX(ReflectionLookup, _cubemap));
				SurfaceColor *= ReflectionMapColor.rgb;
#endif		

				float3 OutColor = float3(0, 0, 0);
				float OutAlpha = _surface_alpha;

// apply diffuse lighting from the vert shader
#if DIRECTION_VERTEX_LIGHTS
				OutColor += SurfaceColor * i.vertLighting.rgb;
#endif 

// apply ambient lighting
#if AMBIENT_VERTEX_LIGHT
				OutColor += SurfaceColor * _ambientvert_light_color.rgb;
#endif

// Direction
#if DIRECTION_SPEC_LIGHT
				{
					float3 LightColor = _dirspec_light_color.rgb;
					float3 LightDirection = normalize(-_dirspec_light_direction.xyz);

					float3 SpecColor = _dirspec_light_spec_color;
					float SpecularPower = _dirspec_light_spec_k;

					float3 h = normalize(LightDirection + ViewDirection);

					float diff = max(0, dot(Normal, LightDirection));

					float nh = max(0, dot(Normal, h));

					float spec = pow(nh, SpecularPower*128.0);

					float Intensity = _dirspec_light_color.a;

					OutColor += (SurfaceColor * LightColor * diff + LightColor * SpecColor * spec) * Intensity;
				}
#endif

// Point
#if POINT_SPEC_LIGHT
				{
					float3 LightColor = _pointspec_light_color.rgb;
					float3 LightPosition = _pointspec_light_position.xyz;
					float3 LightDirection = normalize(LightPosition - i.posWorld.xyz);
					float3 LightDistance = length(LightPosition - i.posWorld.xyz);
					float LightRadius = _pointspec_light_radius;

					float3 SpecColor = _pointspec_light_spec_color;
					float SpecularPower = _pointspec_light_spec_k;

					float3 h = normalize(LightDirection + ViewDirection);

					float diff = max(0, dot(Normal, LightDirection));

					float nh = max(0, dot(Normal, h));

					float spec = pow(nh, SpecularPower*128.0);

					float r = LightDistance / LightRadius;

					float Atten = saturate(1.0 / (1.0 + 25.0*r*r) * saturate((1 - r) * 5.0));

					float Intensity = _pointspec_light_color.a;

					OutColor += Atten * (SurfaceColor * LightColor * diff + LightColor * SpecColor * spec) * Intensity;
				}
#endif

// Emissive texture map
#if EMISSION_MAP
				float4 emissiveSample = tex2D(_emissionmap, i.uv);
				OutColor += SurfaceColor * emissiveSample.a * _emission_color.rgb * _emission_intensity;
#endif

// Apply height and distance fog to the final color
#if DISTANCE_FOG				
				float dist = length(i.posWorld.xyz - _WorldSpaceCameraPos.xyz);
				float distFogStrength =  clamp(pow(dist / _distfog_params.x, _distfog_params.z) * _distfog_params.y, 0, 1);
				OutColor = lerp(OutColor, _distfog_color, distFogStrength);
#endif
				
#if HEIGHT_FOG
				float heightFogStrength = clamp(InvLerp(_heightfog_top, _heightfog_bottom, i.posWorld.y), 0, 1);
				OutColor = lerp(OutColor, _heightfog_color, heightFogStrength);
#endif

// We're done!
				return float4(OutColor, OutAlpha);
			}
			ENDCG
		}
    }
    FallBack "Diffuse"
    //CustomEditor "ShaderForgeMaterialInspector"
}
